<?php

namespace SaC\Helper;

use SaC\Config\TunnelConfig;
use Symfony\Component\Process\Process;

class TunnelHelper {
	private $process;

	/**
	 * Creates an SSH tunnel using provided tunnel config
	 *
	 * @param array $tunnelConfig
	 * @return void
	 */
	public function createSSHTunnel($tunnelConfig){

		$tunnelString =
			"ssh -N -L ".
			$tunnelConfig[TunnelConfig::FIELD_PORT_TO_BIND].
			":".
			$tunnelConfig[TunnelConfig::FIELD_MYSQL_HOST].
			":".
			$tunnelConfig[TunnelConfig::FIELD_MYSQL_PORT].
			" ";

		if (
			isset($tunnelConfig[TunnelConfig::FIELD_USER]) &&
			$tunnelConfig[TunnelConfig::FIELD_USER]
		){
			$tunnelString .= ' '.
				$tunnelConfig[TunnelConfig::FIELD_USER].
				'@';
		}

		$tunnelString .= $tunnelConfig[TunnelConfig::FIELD_HOST];
		echo "Tunneling with command: $tunnelString\n";

		$this->process = new Process($tunnelString);
		$this->process->setTimeout(60);
		$this->process->start();
		echo "Waiting to open\n";
		sleep(5);

		echo "Tunnel created with\n";
	}

	public function __destruct(){
		echo "Closing tunnel\n";
		$this->process->stop(3);
	}
}
