<?php

namespace SaC\Helper;

use Symfony\Component\Finder\Finder;


class FinderHelper {

	const FILETYPE_YAML = 'yaml';

	private $finder;

	public function __construct(){
		$this->finder = new Finder;
	}

	/**
	 * Returns a Finder of all of the files in a given directory that are of the given type.
	 *
	 * @param String $directory
	 * @param String $fileType
	 * @param int $depth
	 * @return Finder
	 */
	public function getAllFilesInDirectoryByType(string $directory, String $fileType, int $depth = 0) : Finder{
		return $this->finder->files()->in($directory)->name('*.'.$fileType)->depth($depth);
	}
}
