<?php

namespace SaC\Helper;

use Symfony\Component\Yaml\Yaml;


class YamlHelper {
	/**
	 * Parses the given Yaml file into an associative array
	 *
	 * @param String $filePath
	 * @return array
	 */
	public static function parse(String $filePath) : Array{
		$parsedYaml = Yaml::parseFile($filePath);

		if (!$parsedYaml){
			return [];
		}

		return $parsedYaml;
	}
}
