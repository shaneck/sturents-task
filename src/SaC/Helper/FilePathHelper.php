<?php

namespace SaC\Helper;

class FilePathHelper {
	/**
	 * Converts a filepath given as an option into an absolutepath
	 * If the given path is relative, it prepends the current working
	 * directory to it, otherwise if it's absolute it uses the given
	 * filepath.
	 *
	 * @param string $filePath
	 * @return string
	 */
	public static function getAbsoluteFilePathFromOption(String $filePath) : String{
		if ($filePath && $filePath[0]!='/'){
			$filePath = getcwd().'/'.$filePath;
		}
		elseif (!$filePath) {
			$filePath = getcwd();
		}

		return $filePath;
	}
}
