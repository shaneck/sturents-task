<?php

namespace SaC\Helper;

class DirectoryHelper {
	/**
	 * Converts a directory given as an option into an absolutepath
	 * If the given path is relative, it prepends the current working
	 * directory to it, otherwise if it's absolute it uses the given
	 * directory.
	 *
	 * @param string $directory
	 * @return string
	 */
	public static function getDirectoryFromOption(string $directory = null) : String{
		if ($directory && $directory[0]!='/'){
			$directory = getcwd().'/'.$directory;
		}
		elseif (!$directory) {
			$directory = getcwd();
		}

		return $directory;
	}
}
