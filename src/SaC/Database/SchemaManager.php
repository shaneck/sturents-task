<?php

namespace SaC\Database;

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\Comparator;
use SaC\Config\DatabaseConfig;


class SchemaManager {
	private $conn;

	public function __construct($databaseConfig){
		$config = new Configuration();

		if (!isset($databaseConfig[DatabaseConfig::FIELD_PORT])){
			$databaseConfig[DatabaseConfig::FIELD_PORT] = 3306;
		}

		$connectionParams = [
			'dbname' => $databaseConfig[DatabaseConfig::FIELD_DBNAME],
			'user' => $databaseConfig[DatabaseConfig::FIELD_USER],
			'password' => $databaseConfig[DatabaseConfig::FIELD_PASSWORD],
			'host' => $databaseConfig[DatabaseConfig::FIELD_HOST],
			'port' => $databaseConfig[DatabaseConfig::FIELD_PORT],
			'driver' => 'pdo_mysql',
		];
		$this->conn = DriverManager::getConnection($connectionParams, $config);
	}

	/**
	 * Takes an array of queries and applies them one by one.
	 *
	 * @param array $queries
	 * @return void
	 */
	public function applyQueries(Array $queries){
		foreach ($queries as $query){
			$this->applyQuery($query);
		}
	}

	/**
	 * Applies a query to the database
	 *
	 * @param String $query
	 * @return void
	 */
	public function applyQuery(String $query){
		$this->conn->executeQuery($query);
	}

	/**
	 * Compares the fromSchema with the toSchema and returns the queries
	 * that need to be ran to make them equal.
	 *
	 * @param Schema $fromSchema
	 * @param Schema $toSchema
	 * @return array $queries
	 */
	public function compareSchemas(
		Schema $fromSchema,
		Schema $toSchema
	) : Array{
		$comparator = new Comparator();

		$schemaDiff = $comparator->compare($fromSchema, $toSchema);

		return $schemaDiff->toSaveSql($this->conn->getDatabasePlatform());
	}

	/**
	 * Converts a database into a doctrine Schema.
	 *
	 * @return Schema
	 */
	public function getCurrentSchema() : Schema{
		$sm = $this->conn->getSchemaManager();

		$schema = $sm->createSchema();

		return $schema;
	}

	/**
	 * Takes an array of parsed tables and converts them into a doctrine Schema
	 *
	 * @param array $parsedTables
	 * @return Schema
	 */
	public function buildSchemaFromArray(Array $parsedTables) : Schema{
		$schema = new Schema();

		foreach ($parsedTables as $parsedTable){
			$myTable = $schema->createTable($parsedTable['name']);

			$parsedTableSchema = $parsedTable['schema'];

			foreach ($parsedTableSchema['fields'] as $fieldName => $options){
				$type = $options['type'];
				if ($type=='int'){
					$type = 'integer';
				}
				if ($type=='varchar'){
					$type = 'string';
				}
				unset($options['type']);
				$myColumn = $myTable->addColumn($fieldName, $type, $options);

				if (isset($options['auto']) && $options['auto']===1){
					$myColumn->setAutoincrement(true);
				}
			}

			$myTable->setPrimaryKey([$parsedTableSchema['primary']]);

			foreach ($parsedTableSchema['index'] as $index){
				$myTable->addIndex([$index], $index);
			}
		}

		return $schema;
	}
}
