<?php

namespace SaC\Config;

use SaC\Interfaces\ConfigInterface;

class TunnelConfig implements ConfigInterface {
	const FIELD_USER = 'user';
	const FIELD_HOST = 'host';
	const FIELD_PORT_TO_BIND = 'port_to_bind';
	const FIELD_MYSQL_HOST = 'mysql_host';
	const FIELD_MYSQL_PORT = 'mysql_port';

	private static $requiredFields = [
		self::FIELD_HOST,
		self::FIELD_PORT_TO_BIND,
		self::FIELD_MYSQL_HOST,
		self::FIELD_MYSQL_PORT,
	];

	/**
	 * Validates that the required fields exist
	 *
	 * @param array $tunnelConfig
	 * @return bool
	 */
	public static function validate($tunnelConfig){
		foreach (self::$requiredFields as $requiredField){
			if (!isset($tunnelConfig[$requiredField])){
				return false;
			}
		}

		return true;
	}
}
