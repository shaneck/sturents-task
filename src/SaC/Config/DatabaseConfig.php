<?php

namespace SaC\Config;

use SaC\Interfaces\ConfigInterface;

class DatabaseConfig implements ConfigInterface {
	const FIELD_DBNAME = 'dbname';
	const FIELD_USER = 'user';
	const FIELD_PASSWORD = 'password';
	const FIELD_HOST = 'host';
	const FIELD_PORT = 'port';

	private static $requiredFields = [
		self::FIELD_USER,
		self::FIELD_PASSWORD,
		self::FIELD_HOST,
	];

	/**
	 * Validates that the required fields exist
	 *
	 * @param array $databaseConfig
	 * @return bool
	 */
	public static function validate($databaseConfig){
		foreach (self::$requiredFields as $requiredField){
			if (!isset($databaseConfig[$requiredField])){
				return false;
			}
		}

		return true;
	}
}
