<?php

namespace SaC\Interfaces;

interface ConfigInterface {
	public static function validate($config);
}
