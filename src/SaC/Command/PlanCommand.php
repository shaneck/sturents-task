<?php

namespace SaC\Command;

use Symfony\Component\Console\Command\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

use Symfony\Component\Console\Output\OutputInterface;

use SaC\Database\SchemaManager;
use SaC\Helper\FinderHelper;
use SaC\Helper\YamlHelper;
use SaC\Helper\DirectoryHelper;
use SaC\Helper\FilePathHelper;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use SaC\Helper\TunnelHelper;
use SaC\Config\DatabaseConfig;
use SaC\Config\TunnelConfig;
use Symfony\Component\Finder\SplFileInfo;


class PlanCommand extends Command {
	/**
	 * @var OutputInterface
	 */
	private $output;

	protected function configure(){
		$this
			->setName('plan')
			->addOption('config', null, InputOption::VALUE_REQUIRED, 'Config file containing database connection info')
			->addOption('directory', null, InputOption::VALUE_REQUIRED, 'Directory containing schema files')
			->addOption('database', null, InputOption::VALUE_OPTIONAL, 'Database name')
			->addOption('tunnel', null, InputOption::VALUE_NONE, 'Sets up an SSH tunnel to connect to MySQL')
			->setDescription('Shows planned changes to schema');
	}

	protected function execute(InputInterface $input, OutputInterface $output){
		$this->output = $output;
		$configOptionValue = $input->getOption('config');

		if (!$configOptionValue){
			return $this->outputError( "The --config option is missing, it should be a path to a config YAML file. See the Readme for more information.");
		}

		$configFile = FilePathHelper::getAbsoluteFilePathFromOption($configOptionValue);

		$config = YamlHelper::parse($configFile);

		if (!$config || !isset($config['database'])){
			return $this->outputError("The config file provided was not valid or didn't include a database. See the Readme for more information.");
		}

		$directoryOptionValue = $input->getOption('directory');

		if (!$directoryOptionValue){
			return $this->outputError( "The --directory option is missing, it should be a path to a directory of database table YAML files. See the Readme for more information.");
		}

		$databaseConfig = $config['database'];
		$databaseOptionValue = $input->getOption('database');
		if ($databaseOptionValue){
			$databaseConfig[DatabaseConfig::FIELD_DBNAME] = $databaseOptionValue;
		}

		if (!DatabaseConfig::validate($databaseConfig)){
			return $this->outputError("Database config incorrectly provided. See the Readme for more information.");
		}

		$tunnelOptionValue = $input->getOption('tunnel');
		if ($tunnelOptionValue){
			$tunnelConfig = $config['tunnel'];
			if (!TunnelConfig::validate($tunnelConfig)){
				return $this->outputError("Tunnel config incorrectly provided with --tunnel option. See the Readme for more information.");
			}
			$th = new TunnelHelper;
			$th->createSSHTunnel($tunnelConfig);

			$databaseConfig[DatabaseConfig::FIELD_HOST] = '127.0.0.1';
			$databaseConfig[DatabaseConfig::FIELD_PORT] = $tunnelConfig['port_to_bind'];
		}

		$schemaManager = new SchemaManager($databaseConfig);
		$directory = DirectoryHelper::getDirectoryFromOption($directoryOptionValue);

		$finderHelper = new FinderHelper;

		$yamlFiles = $finderHelper->getAllFilesInDirectoryByType($directory, FinderHelper::FILETYPE_YAML);

		if (!$yamlFiles->hasResults()){
			return $this->outputError("No yaml files found in the directory: '{$directoryOptionValue}'. Specify this directory with the --directory option.");
		}

		$parsedTables = [];
		foreach ($yamlFiles as $yamlFile){
			/**
			 * @var SplFileInfo $yamlFile
			 */
			$parsedTable = YamlHelper::parse($yamlFile->getPathName());

			$tableName = explode('.', $yamlFile->getFileName())[0];

			$parsedTables[] = [
				'name' => $tableName,
				'schema' => $parsedTable,
			];
		}

		$fromSchema = $schemaManager->getCurrentSchema();

		$toSchema = $schemaManager->buildSchemaFromArray($parsedTables);

		$queries = $schemaManager->compareSchemas($fromSchema, $toSchema);

		if (!$queries){
			return $this->outputInfo("No changes detected, schema is up to date!");
		}

		$this->outputInfo("Schema changes detected, the following queries will need to be run:");
		foreach ($queries as $query){
			$output->writeln("\n{$query};");
		}
		$helper = $this->getHelper('question');
		$question = new ConfirmationQuestion("\n<question>Would you like to apply these changes automatically (type 'yes' to confirm, anything else exits)?</question> ", false);
		if (!$helper->ask($input, $output, $question)){
			return $this->outputInfo("Exiting!");
		}

		$schemaManager->applyQueries($queries);

		return $this->outputInfo("Changes applied, exiting!");
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	private function outputError(string $message){
		return $this->output->writeln("<error>$message</error>");
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	private function outputInfo(string $message){
		return $this->output->writeln("<info>$message</info>");
	}
}
