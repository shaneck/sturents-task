# StuRents-Task

## Checklist
* Stage 1 - Complete
* Stage 2 - Complete
* Stage 3 - Complete but untested
* Stage 4 - Complete
* Stage 5 - Not attempted

## Other considerations
* The PlanCommand execute function, could do with a lot of cleanup/extraction
* I ran out of time to test the SSH Tunneling was working, but I am relatively confident it will work.
* I researched Stage5 briefly but could not come up with a clean solution that would avoid false positives.
* I also should add some validation to the Yaml table files that are parsed, similar to how I validated the config.yaml

## How to use
Run `./sac plan` this is where all of the logic lies.
Upon running it will compare the differences and then prompt the user to enter 'yes' if they want to automatically apply the changes.

Example use: `./sac plan --directory tables --database testing --config config.yaml`

## Options
* --directory Specify which directory your schema yaml files are located example `--directory tables/` this works with relative and absolute paths. Default: Current Directory
* --database Specfiy the name of the database you wish to use. Default: dbname supplied in config file
* --config **(REQUIRED)** Specify which config file contains your database/tunnel config. Relative and absolute paths work. 
* --tunnel Will use the tunnel config provided in the config file to connect to mysql via SSH Tunnel

Example config.yaml is provided.

